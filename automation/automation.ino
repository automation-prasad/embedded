
#include <ESP8266WiFi.h>

#ifndef STASSID
#define STASSID "my-wifi-net"
#define STAPSK  "12345678"
#endif

#define MAX_SRV_CLIENTS 2
const char* ssid     = STASSID;
const char* password = STAPSK;

WiFiServer server(80);


/////////////////////////////////////////////////////////////////////////////////
//  TCP BUFFER STUFF
//////////////////////////////////////////////////////////////////////////////////
int packet_counter = 0;
// we receive byte array from app
boolean buffer_ready = false;
byte readbuf[4];
uint8_t dataBuffer[2][4];
uint8_t cnt[2] ;
//////////////////////////////////////////////////////////////////////////////////

WiFiClient serverClients[MAX_SRV_CLIENTS];

#define RELAY1 2
#define RELAY2 3
#define RELAY3 4
#define RELAY4 5

uint8_t relay_array[4] = {RELAY1,RELAY2,RELAY3,RELAY4};
void setup() {
  Serial.begin(115200);
  pinMode(RELAY1,OUTPUT);
  pinMode(RELAY2,OUTPUT);
  pinMode(RELAY3,OUTPUT);
  pinMode(RELAY4,OUTPUT);

  // We start by connecting to a WiFi network

  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  digitalWrite(2,LOW);
  digitalWrite(3,LOW);
  digitalWrite(4,LOW);
  digitalWrite(5,LOW);

}

void loop() {
  StartNormalMode();
}

void InitNormalMode() {
  WiFi.mode(WIFI_STA);
  Serial.println(ssid); Serial.println(password);
  WiFi.begin(ssid, password);
  uint8_t wificounter = 0;
  while (WiFi.status() != WL_CONNECTED && wificounter < 20) {
    Serial.print(".");
    delay(500);
    wificounter++;
  }
  if (wificounter >= 20) {
    Serial.printf("Could not connect to "); Serial.println(ssid);
  } else {
    // extract IP into uint8_t
    uint8_t IPB1, IPB2, IPB3, IPB4;
    IPAddress dynamicIP = WiFi.localIP();
    IPB4 = dynamicIP[0]; // MSB
    IPB3 = dynamicIP[1];
    IPB2 = dynamicIP[2];
    IPB1 = dynamicIP[3]; // LSB
    printf("Dynamic IP assigned : %d.%d.%d.%d\n", IPB4, IPB3, IPB2, IPB1);

    // Disconnect to the wifi and reconnect using new Static IP
    WiFi.disconnect();
    delay(100);
    // Setup IP Details for connection
    IPAddress staticIP(IPB4, IPB3, IPB2, 199);
    IPAddress gateway(IPB4, IPB3, IPB2, 1);
    IPAddress subnet(255, 255, 0, 0);
    
    LABEL_INIT_NORMAL:
    // reconnect to wifi
    if (WiFi.status() != WL_CONNECTED) {
      WiFi.persistent(false);
      WiFi.mode(WIFI_OFF);
      WiFi.mode(WIFI_STA);
    
      if (!WiFi.config(staticIP, gateway, subnet)) {//, primaryDNS, secondaryDNS)) {
        Serial.println("STA Failed to configure with static IP");
      } else {
        printf("STA configured successfully with static IP : %d.%d.%d.%d\n", staticIP[0], staticIP[1], staticIP[2], staticIP[3]);
      }
      WiFi.begin(ssid, password);
    } else {
      printf("Wifi is not disconnected, Retrying\n");
      WiFi.disconnect();
      delay(100);
      goto LABEL_INIT_NORMAL;
    }
    
    wificounter = 0;
    while (WiFi.status() != WL_CONNECTED && wificounter < 20) {
      delay(500);
      Serial.print(".");
      wificounter++;
    }

    if (wificounter >= 20) {
      goto LABEL_INIT_NORMAL;
    }
    
    printf("\nWiFi connected.\n");
    printf("IP address: ");
    Serial.println(WiFi.localIP());
    server.begin();
    server.setNoDelay(true);
    StartNormalMode();
  }
}

void WIFI_Connect() {
  WiFi.disconnect();
  printf("Reconnecting WiFi...\n");
  InitNormalMode();
}

void parseData(uint8_t data[])
{
  if(data[0] == '*'){
      Serial.println("Data is valid");
      for (int j = 0; j < 4; j++)
      {
        Serial.println(data[j]);
      }
      if(data[2]){
        digitalWrite(relay_array[data[1] - 1],LOW);
      }
      else{
        digitalWrite(relay_array[data[1] - 1],HIGH);
      }    
  }
  else if(data[0] == '#'){
      Serial.println("Data is valid for request device state");
      for (int j = 0; j < 4; j++)
      {
        Serial.println(data[j]);
      }  
  }
}

void startServer()
{
   unsigned long currentMillis = millis();
//   Serial.println(WiFi.localIP());
   uint8_t i;
    //check if there are any new clients
    if (server.hasClient())
    {
      for (i = 0; i < MAX_SRV_CLIENTS; i++)
      {
        //find free/disconnected spot
        if (!serverClients[i] || !serverClients[i].connected())
        {
          if (serverClients[i]) serverClients[i].stop();
          serverClients[i] = server.available();
          continue;
        }
      }
      //no free/disconnected spot so reject
    WiFiClient serverClient = server.available();
    serverClient.stop();
  }
  uint8_t sbuf[2];

  sbuf[0] = 'A';
  sbuf[1] = 'B';
   for (i = 0; i < MAX_SRV_CLIENTS; i++)
   {
      if (serverClients[i] && serverClients[i].connected() && serverClients[i].available()) 
      {
        uint8_t c = serverClients[i].read();
        dataBuffer[i][cnt[i]] = c;
//        Serial.print("count for ");
//        Serial.print(i);
//        Serial.print(" : ");
//        Serial.println(cnt[i]);
        cnt[i]++;

        if(cnt[i] > 3 ){
            Serial.println("Buffer Full");
            for (int j = 0; j < 4; j++)
            {
//              Serial.println(dataBuffer[i][j]);
            }
            cnt[i] = 0;
           parseData(dataBuffer[i]);
        }
        delay(1);
      }
    }
}



void StartNormalMode() {
  uint8_t i;
  if (WiFi.status() != WL_CONNECTED) {
    WIFI_Connect();
  } else {
    startServer();   
  }
}
